"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
// Global protocol APIs.
const protocol = process.electronBinding('protocol');
// Fallback protocol APIs of default session.
Object.setPrototypeOf(protocol, new Proxy({}, {
    get(_target, property) {
        if (!electron_1.app.isReady())
            return;
        const protocol = electron_1.session.defaultSession.protocol;
        if (!Object.getPrototypeOf(protocol).hasOwnProperty(property))
            return;
        // Returning a native function directly would throw error.
        return (...args) => protocol[property](...args);
    },
    ownKeys() {
        if (!electron_1.app.isReady())
            return [];
        return Object.getOwnPropertyNames(Object.getPrototypeOf(electron_1.session.defaultSession.protocol));
    },
    getOwnPropertyDescriptor() {
        return { configurable: true, enumerable: true };
    }
}));
exports.default = protocol;
