"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ipc_main_internal_1 = require("@electron/internal/browser/ipc-main-internal");
const errorUtils = require("@electron/internal/common/error-utils");
const callHandler = async function (handler, event, args, reply) {
    try {
        const result = await handler(event, ...args);
        reply([null, result]);
    }
    catch (error) {
        reply([errorUtils.serialize(error)]);
    }
};
exports.handle = function (channel, handler) {
    ipc_main_internal_1.ipcMainInternal.on(channel, (event, requestId, ...args) => {
        callHandler(handler, event, args, responseArgs => {
            if (requestId) {
                event._replyInternal(`${channel}_RESPONSE_${requestId}`, ...responseArgs);
            }
            else {
                event.returnValue = responseArgs;
            }
        });
    });
};
let nextId = 0;
function invokeInWebContents(sender, sendToAll, command, ...args) {
    return new Promise((resolve, reject) => {
        const requestId = ++nextId;
        const channel = `${command}_RESPONSE_${requestId}`;
        ipc_main_internal_1.ipcMainInternal.on(channel, function handler(event, error, result) {
            if (event.sender !== sender) {
                console.error(`Reply to ${command} sent by unexpected WebContents (${event.sender.id})`);
                return;
            }
            ipc_main_internal_1.ipcMainInternal.removeListener(channel, handler);
            if (error) {
                reject(errorUtils.deserialize(error));
            }
            else {
                resolve(result);
            }
        });
        if (sendToAll) {
            sender._sendInternalToAll(command, requestId, ...args);
        }
        else {
            sender._sendInternal(command, requestId, ...args);
        }
    });
}
exports.invokeInWebContents = invokeInWebContents;
