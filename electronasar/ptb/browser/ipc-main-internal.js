"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const emitter = new events_1.EventEmitter();
// Do not throw exception when channel name is "error".
emitter.on('error', () => { });
exports.ipcMainInternal = emitter;
