"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
const v8Util = process.electronBinding('v8_util');
const isMac = process.platform === 'darwin';
exports.setDefaultApplicationMenu = () => {
    if (v8Util.getHiddenValue(global, 'applicationMenuSet'))
        return;
    const helpMenu = {
        role: 'help',
        submenu: [
            {
                label: 'Learn More',
                click: async () => {
                    await electron_1.shell.openExternal('https://electronjs.org');
                }
            },
            {
                label: 'Documentation',
                click: async () => {
                    await electron_1.shell.openExternal(`https://github.com/electron/electron/tree/v${process.versions.electron}/docs#readme`);
                }
            },
            {
                label: 'Community Discussions',
                click: async () => {
                    await electron_1.shell.openExternal('https://discuss.atom.io/c/electron');
                }
            },
            {
                label: 'Search Issues',
                click: async () => {
                    await electron_1.shell.openExternal('https://github.com/electron/electron/issues');
                }
            }
        ]
    };
    const macAppMenu = { role: 'appMenu' };
    const template = [
        ...(isMac ? [macAppMenu] : []),
        { role: 'fileMenu' },
        { role: 'editMenu' },
        { role: 'viewMenu' },
        { role: 'windowMenu' },
        helpMenu
    ];
    const menu = electron_1.Menu.buildFromTemplate(template);
    electron_1.Menu.setApplicationMenu(menu);
};
