"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function electronBindingSetup(binding, processType) {
    return function electronBinding(name) {
        try {
            return binding(`atom_${processType}_${name}`);
        }
        catch (error) {
            if (/No such module/.test(error.message)) {
                return binding(`atom_common_${name}`);
            }
            else {
                throw error;
            }
        }
    };
}
exports.electronBindingSetup = electronBindingSetup;
