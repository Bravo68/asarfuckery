"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ipc_renderer_internal_utils_1 = require("@electron/internal/renderer/ipc-renderer-internal-utils");
window.onload = function () {
    // Use menu API to show context menu.
    window.InspectorFrontendHost.showContextMenuAtPoint = createMenu;
    // correct for Chromium returning undefined for filesystem
    window.Persistence.FileSystemWorkspaceBinding.completeURL = completeURL;
    // Use dialog API to override file chooser dialog.
    window.UI.createFileSelectorElement = createFileSelectorElement;
};
// Extra / is needed as a result of MacOS requiring absolute paths
function completeURL(project, path) {
    project = 'file:///';
    return `${project}${path}`;
}
// The DOM implementation expects (message?: string) => boolean
window.confirm = function (message, title) {
    return ipc_renderer_internal_utils_1.invokeSync('ELECTRON_INSPECTOR_CONFIRM', message, title);
};
const useEditMenuItems = function (x, y, items) {
    return items.length === 0 && document.elementsFromPoint(x, y).some(function (element) {
        return element.nodeName === 'INPUT' ||
            element.nodeName === 'TEXTAREA' ||
            element.isContentEditable;
    });
};
const createMenu = function (x, y, items) {
    const isEditMenu = useEditMenuItems(x, y, items);
    ipc_renderer_internal_utils_1.invoke('ELECTRON_INSPECTOR_CONTEXT_MENU', items, isEditMenu).then(id => {
        if (typeof id === 'number') {
            window.DevToolsAPI.contextMenuItemSelected(id);
        }
        window.DevToolsAPI.contextMenuCleared();
    });
};
const showFileChooserDialog = function (callback) {
    ipc_renderer_internal_utils_1.invoke('ELECTRON_INSPECTOR_SELECT_FILE').then(([path, data]) => {
        if (path && data) {
            callback(dataToHtml5FileObject(path, data));
        }
    });
};
const dataToHtml5FileObject = function (path, data) {
    return new File([data], path);
};
const createFileSelectorElement = function (callback) {
    const fileSelectorElement = document.createElement('span');
    fileSelectorElement.style.display = 'none';
    fileSelectorElement.click = showFileChooserDialog.bind(this, callback);
    return fileSelectorElement;
};
