"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const binding = process.electronBinding('ipc');
const v8Util = process.electronBinding('v8_util');
// Created by init.js.
exports.ipcRendererInternal = v8Util.getHiddenValue(global, 'ipc-internal');
const internal = true;
exports.ipcRendererInternal.send = function (channel, ...args) {
    return binding.ipc.send(internal, channel, args);
};
exports.ipcRendererInternal.sendSync = function (channel, ...args) {
    return binding.ipc.sendSync(internal, channel, args)[0];
};
exports.ipcRendererInternal.sendTo = function (webContentsId, channel, ...args) {
    return binding.ipc.sendTo(internal, false, webContentsId, channel, args);
};
exports.ipcRendererInternal.sendToAll = function (webContentsId, channel, ...args) {
    return binding.ipc.sendTo(internal, true, webContentsId, channel, args);
};
