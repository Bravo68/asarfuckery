"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
const ipcRendererUtils = require("@electron/internal/renderer/ipc-renderer-internal-utils");
exports.webFrameInit = () => {
    // Call webFrame method
    ipcRendererUtils.handle('ELECTRON_INTERNAL_RENDERER_WEB_FRAME_METHOD', (event, method, ...args) => {
        // The TypeScript compiler cannot handle the sheer number of
        // call signatures here and simply gives up. Incorrect invocations
        // will be caught by "keyof WebFrameMethod" though.
        return electron_1.webFrame[method](...args);
    });
};
