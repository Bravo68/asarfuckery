"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ipc_renderer_internal_1 = require("@electron/internal/renderer/ipc-renderer-internal");
const errorUtils = require("@electron/internal/common/error-utils");
exports.handle = function (channel, handler) {
    ipc_renderer_internal_1.ipcRendererInternal.on(channel, (event, requestId, ...args) => {
        new Promise(resolve => resolve(handler(event, ...args))).then(result => {
            return [null, result];
        }, error => {
            return [errorUtils.serialize(error)];
        }).then(responseArgs => {
            event.sender.send(`${channel}_RESPONSE_${requestId}`, ...responseArgs);
        });
    });
};
let nextId = 0;
function invoke(command, ...args) {
    return new Promise((resolve, reject) => {
        const requestId = ++nextId;
        ipc_renderer_internal_1.ipcRendererInternal.once(`${command}_RESPONSE_${requestId}`, (_event, error, result) => {
            if (error) {
                reject(errorUtils.deserialize(error));
            }
            else {
                resolve(result);
            }
        });
        ipc_renderer_internal_1.ipcRendererInternal.send(command, requestId, ...args);
    });
}
exports.invoke = invoke;
function invokeSync(command, ...args) {
    const [error, result] = ipc_renderer_internal_1.ipcRendererInternal.sendSync(command, null, ...args);
    if (error) {
        throw errorUtils.deserialize(error);
    }
    else {
        return result;
    }
}
exports.invokeSync = invokeSync;
